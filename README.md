# Bocklists
This is a collection of Blocklists/Blacklists I'm using by myself.
Samsung SmartTV

### Microsoft
[![Microsoft](https://badgen.net/badge/Microsoft/Experimental/red?icon=windows)](https://codeberg.org/Antares/Bocklists/raw/branch/master/microsoft.txt)

Blocklist for PiHole to Block Microsoft Tracing

### Smart TV
[![SmartTV](https://badgen.net/badge/SmartTV/Experimental/red)](https://codeberg.org/Antares/Bocklists/raw/branch/master/smarttv.txt)

Blocklist for PiHole to Block Tracking and information leaking on Samsung SmartTV & HBBTV; Developed and tested with a Samsung UE32ES6750
